<?php
function ubah_huruf($string){
//kode di sini
$mulaiDariA = [];
$mulaiDariB = [];
foreach (range('a', 'z')as $char) {
    # code...
    array_push($mulaiDariA, $char);
    array_push($mulaiDariB, $char);
}
array_shift($mulaiDariB);
array_push($mulaiDariB, "a");

$strABC = implode($mulaiDariA);
$strBCD = implode($mulaiDariB);

echo strtr($string, $strABC, $strBCD);
}




// TEST CASES
echo ubah_huruf('wow') . "<br>" ; // xpx
echo ubah_huruf('developer') . "<br>" ; // efwfmpqfs
echo ubah_huruf('laravel') . "<br>"; // mbsbwfm
echo ubah_huruf('keren') . "<br>"; // lfsfo
echo ubah_huruf('semangat') . "<br>"; // tfnbohbu